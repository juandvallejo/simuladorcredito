// amofran_es - Only ES
function check() { 
	var loanamt = numericVal(document.calcform.amt.value);
	var paymnt = numericVal(document.calcform.pay.value);
	var rate = numericVal(document.calcform.rate.value);
	if(loanamt=="" || isNaN(parseFloat(loanamt)) || loanamt==0)
	 { alert("NOTICE : \n Enter a valid loan amount.\n Introduzca el valor del principal valido\n Digite o valor do principal valido"); 
		document.calcform.amt.value="";
		document.calcform.amt.focus();
		return false; 
	} else if(paymnt=="" || isNaN(parseFloat(paymnt)) || paymnt==0)
	 { alert("NOTICE : \n Enter a valid number of payments.\n Introduzca el numero  de pagos valido\n Digite o numero de pagamentos  valido"); 
		document.calcform.pay.value="";
		document.calcform.pay.focus();
		return false; 
	} else if(rate=="" || isNaN(parseFloat(rate)) || rate==0)
	 { alert("NOTICE : \n Enter a valid interest rate.\n Introduzca la tasa de interes valida\n Digite a taxa de juros valida"); 
		document.calcform.rate.value="";
		document.calcform.rate.focus();
		return false; 
	} else {
		calc(); 
		show();
	}
}
function pmtValue(pv,n,i){return pmt= pv* i*( Math.pow( 1 + i ,n )) / ( Math.pow( 1 + i , n ) - 1);	}
function calc()
{
	var pv = numericVal(document.calcform.amt.value);
	var n   = numericVal(document.calcform.pay.value);
	var i  = numericVal(document.calcform.rate.value)/100;
	var pmt = pmtValue(pv,n,i);
	document.calcform.pmt.value = preFormat((pmt),2);
    document.calcform.Totint.value = preFormat(((pmt*n)-pv),2);
	document.calcform.Totpay.value = preFormat(((pmt*n)),2);
  }	
function show() {
	var amount=numericVal(document.calcform.amt.value);
	var numpay=numericVal(document.calcform.pay.value);
	var rate=numericVal(document.calcform.rate.value);
 	rate=rate/100;
	var monthly=rate;
	var payment=((amount*monthly)/(1-Math.pow((1+monthly),-numpay)));
	var total=payment*numpay;
	var interest=total-amount;
	var detail = "";
	detail += "<table border='0' align='center' cellpadding='5' cellspacing='0' width='90%' style='font-family:courier;font-size:12px'> \
			<tr><td bgcolor='#eeeee0'align='center' valign='bottom' bgcolor='#eeeee0'><b>Mes</b></td><td bgcolor='#eeeee0'align='right' valign='bottom' bgcolor='#eeeee0'><b>Amortizac&iacute;on</b></td> \
			<td bgcolor='#eeeee0'align='right' valign='bottom' bgcolor='#eeeee0'><b>Intereses</b></td><td align='right' valign='bottom' bgcolor='#eeeee0'><b>Pago</b></td> \
			<td bgcolor='#eeeee0'align='right' valign='bottom' bgcolor='#eeeee0'><b>Saldo</b></td></tr><tr><td align='center' bgcolor='#eeeee0'>0</td> \
			<td bgcolor='#eeeee0'align='center' bgcolor='#eeeee0'>&nbsp;</td><td align='center' bgcolor='#eeeee0'>&nbsp;</td><td align='center' bgcolor='#eeeee0'>&nbsp;</td> \
			<td bgcolor='#eeeee0'align='right' bgcolor='#eeeee0'>"+preFormat((amount),2)+"</td></tr>";
	balance=amount;
	var i = 1;
	while (i <= numpay) {
		newInterest=monthly*balance;
		amort=payment-newInterest;
		balance=balance-amort;
		detail += "<tr><td bgcolor='#eeeee0'align='center'>"+i+"</td><td align='right' bgcolor='#eeeee0'>"+preFormat((amort),2)+"</td> \
		    	<td align='right' bgcolor='#eeeee0'>"+preFormat((newInterest),2) +"</td> \
				<td align='right' bgcolor='#eeeee0'>"+preFormat((payment),2)+"</td> \
				<td align='right' bgcolor='#eeeee0'>"+preFormat((balance),2)+"</td></tr>";
		i++;
	}
	detail += "</table>";
	document.getElementById("det").innerHTML = detail;
}
